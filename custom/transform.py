import logging
from whizpy.framework.framework.context_keys import Context_Key as ck
from datetime import datetime


logger = logging.getLogger(__name__)

def transform(ctx_dict):
    logger.info("============= Custom Transformation Logic ==============")
    now=datetime.now().strftime("%H:%M") #
    print(type(now))
    print("Current_time is: ", now)
    ctx_dict[ck.LOAD_DS_LIST.value] = ['patients','shipments']#,'uptravi_penetration','patients','shipments','uptravi_era_percent']
    #### Framework will only load data for datasourcess in this list.
    return ctx_dict
