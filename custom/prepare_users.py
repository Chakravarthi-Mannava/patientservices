import logging

logger = logging.getLogger(__name__)

def prepare_users(ctx_dict):
    logger.info("============= Custom Prepare Users for activate/deactivate Logic ==============")
    return ctx_dict
