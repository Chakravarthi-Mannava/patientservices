import logging

logger = logging.getLogger(__name__)

def extract(ctx_dict):
    logger.info("============= Custom Extraction Logic ==============")
    return ctx_dict
