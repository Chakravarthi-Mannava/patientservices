import logging

logger = logging.getLogger(__name__)

def destination_data_validate(ctx_dict):
    logger.info("============= Custom destination_data_validate Logic ==============")
    return ctx_dict

def final_validation(ctx_dict):
    logger.info("============= Custom E2E Validation Logic ==============")
    return ctx_dict
