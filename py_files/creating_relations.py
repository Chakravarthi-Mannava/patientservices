import pandas as pd
import os
import json
from whizpy.framework.solution.py_files import config
import configparser
from airflow.models import Variable

nlpfiles_dir = 'dags/whizpy/framework/solution/outputs/nlp/askensemble/'
nlpfilessource_dir = 'dags/whizpy/framework/solution/inputs/nlp'

parser = configparser.RawConfigParser()
environment = Variable.get("environment")
if (environment == "PROD"):
    with open('{configpath}/{configfile}'.format(configpath=nlpfilessource_dir,
                                                 configfile=config.prodrelationconfig)) as json_data:
        rel_config = json.load(json_data)
        json_data.close()
else:
    with open('{configpath}/{configfile}'.format(configpath=nlpfilessource_dir,
                                                 configfile=config.relationconfig)) as json_data:
        rel_config = json.load(json_data)
        json_data.close()


def creating_relations():
    # Preparing Relations.csv
    """ This function is used to create Relations Between entities. The First Part of the Code uses the actual Data File
        To Create the Relations. Then We join it With the respective NLP(entities) File to Create the Expected Output"""
    data_final = pd.DataFrame()

    print(nlpfiles_dir)
    print('____________')
    os.chdir(nlpfiles_dir)
    for element in rel_config['relations']:
        temp_master_data = pd.DataFrame()
        for index1, file in enumerate(element['relations_input_file']):
            for parent in element['parents']:
                print(parent['file'])
                parent_nlp=pd.read_csv(parent['file'], sep = "\t",dtype=str)
                parent_nlp.rename(columns={'ID': 'PARENT_DBID', 'CODE': 'PARENT_CODE', 'LEVEL': 'PARENT_LEVEL'},
                                  inplace=True)
                child_nlp = pd.read_csv(element['child']['file'], sep="\t", dtype=str)
                child_nlp.rename(columns={'ID': 'DBID'}, inplace=True)
                cols_to_use = [parent['name'], element['child']['name']]
                temp_data = pd.read_csv(file, sep=rel_config.get('separator', '\t'), usecols=cols_to_use, dtype=str)[cols_to_use]
                print('*****')
                print(temp_data.columns)
                temp_data.columns = ['PARENT_CODE', 'CODE']
                temp_data['PARENT_LEVEL'] = parent['name']
                temp_data['LEVEL'] = element['child']['name']
                for column in temp_data.columns:
                    temp_data[column] = temp_data[column].astype(str)
                    temp_data[column] = temp_data[column].map(lambda x: x.strip())
                temp_data = temp_data.drop_duplicates().reset_index(drop=True)
                print(temp_data.shape)
                temp_data = pd.merge(temp_data, parent_nlp[['PARENT_DBID','PARENT_CODE', 'PARENT_LEVEL']], how="left",
                                    on=["PARENT_CODE", "PARENT_LEVEL"])
                print(temp_data.shape)
                temp_data = pd.merge(temp_data, child_nlp[['DBID', 'CODE', 'LEVEL']], how="left",
                                     on=["CODE","LEVEL"])
                print(temp_data.shape)
                print(temp_data.head(n=5))
                temp_master_data = temp_master_data.append(temp_data)
        data_final = data_final.append(temp_master_data)
    data_final['PARENT_MODEL'] = rel_config['model_name']
    data_final['MODEL'] = rel_config['model_name']
    data_final = data_final[['DBID', 'MODEL', 'CODE', 'LEVEL', 'PARENT_DBID', 'PARENT_MODEL', 'PARENT_CODE',
                             'PARENT_LEVEL']]

    # Exclude Values Logic Starts
    if "exclude_values" in rel_config:
        exclude_val = rel_config["exclude_values"]
        for collist in exclude_val:
            level = collist['level']
            col = list(collist.keys())[0]
            exclude_val_list = list(collist.values())[0]
            data_final = data_final[~(data_final[level].isin(exclude_val_list) & data_final['LEVEL'].isin([col]))]
            if 'exception_list' in collist.keys():
                exceptions = collist['exception_list']
                data_final = data_final[~(data_final['CODE'].isin(exceptions) & data_final['LEVEL'].isin([col]))]
    # Exclude Values Logic Ends

    data_final.to_csv('Relations.csv', sep='\t', index=False)
    print('Relations.csv File Created')