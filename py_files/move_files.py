import requests
import sys
import pandas as pd
import time
from whizpy.framework.solution.py_files import config
import os
import shutil
import numpy as np
import json
def move_files(src_dir, dest_dir):
    try:
        files = os.listdir(src_dir)
        for f in files:
            shutil.copy(src_dir + f, dest_dir)
    except Exception as e:
        print('Failed to move. Reason: {err}'.format(err=e))
        sys.exit(1)