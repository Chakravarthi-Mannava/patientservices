import configparser
import os
from airflow.models import Variable

parser = configparser.RawConfigParser()
environment = Variable.get("environment")
if (environment == "PROD"):
    print("reading PROD Config.ini file")
    parser.read("dags/whizpy/framework/solution/configurations/PRODconfig.ini")
else:
    parser.read("dags/whizpy/framework/solution/configurations/config.ini")

#os.chdir('/usr/local/airflow/dags/emea/pyfiles')
print(parser)
configurations_dir = parser.get('filepath', 'configurations_dir')
base_dir = parser.get('filepath', 'base_dir')

base_data_dir = parser.get('filepath', 'base_data_dir')
druid_files_dir = parser.get('filepath', 'druid_files_dir')
nlpfiles_dir = parser.get('filepath', 'nlpfiles_dir')
nlpfiles_bkp = parser.get('filepath', 'nlpfiles_bkp')
nlpfilessource_dir = parser.get('filepath', 'nlpfilessource_dir')
backup_folder = parser.get('filepath', 'backup_folder')
update_entities_trigger_path = parser.get('filepath', 'update_entities_trigger_path')
host = parser.get('postgres_cred', 'host')
port = parser.get('postgres_cred', 'port')
dbname = parser.get('postgres_cred', 'dbname')
username = parser.get('postgres_cred', 'username')
password = parser.get('postgres_cred', 'password')
druid_ip = parser.get('druidconnection', 'druid_ip')
druid_port = parser.get('druidconnection', 'druid_port')
datasource = parser.get('druidconnection', 'datasource')
retailDataSource_dir = parser.get('filepath', 'retailDataSource_dir')
retailSourceFile = parser.get('filenames', 'retailSourceFile')
sqlfiles_dir = parser.get('filepath', 'sqlfiles_dir')
druid_index_json = parser.get('filenames', 'druid_index_json')
outputcsv = parser.get('filenames', 'outputcsv')
nlpconfig = parser.get('filenames', 'nlpconfig')
relationconfig = parser.get('filenames', 'relationconfig')
prodrelationconfig = parser.get('filenames', 'prodrelationconfig')
nlp_preprocessing_file = parser.get('filenames', 'nlp_preprocessing_file')
web_json = parser.get('filenames', 'web_json')

need_to_transfer = parser.get('druid_file_status', 'need_to_transfer')
druid_load_dir = parser.get('filepath', 'druid_load_dir')

webhook_url = parser.get('notification', 'webhook_url')
notify_druid_ip = parser.get('notification', 'notify_druid_ip')
notify_druid_port = parser.get('notification', 'notify_druid_port')

schedule = parser.get('scheduledinterval', 'schedule')

data_source_variable = parser.get('filenames', 'data_source_variable')

# Data  Validation Variables
druid_validation_dir = parser.get('filepath', 'druid_validation_dir')
druid_validation_result_success_dir = parser.get('filepath', 'druid_validation_result_success_dir')
druid_validation_result_failed_dir = parser.get('filepath', 'druid_validation_result_failed_dir')

# CVM Related Changes Starts From Here

# RedShift Database Variables
redshift_username = parser.get('redshiftconnection', 'username')
redshift_password = parser.get('redshiftconnection', 'password')
redshift_server = parser.get('redshiftconnection', 'server')
redshift_port = parser.get('redshiftconnection', 'port')
redshift_database = parser.get('redshiftconnection', 'database')

# Refresh Pins

rf_login_url = parser.get('refresh_pins', 'rf_login_url')
rf_username = parser.get('refresh_pins', 'rf_username')
rf_password = parser.get('refresh_pins', 'rf_password')
rf_datasource_code = parser.get('refresh_pins', 'rf_datasource_code')
rf_datasources_url = parser.get('refresh_pins', 'rf_datasources_url')
rf_refreshpins_url = parser.get('refresh_pins', 'rf_refreshpins_url')

# Update Entities

ue_url = parser.get('update_entities', 'ue_url')
ue_models = parser.get('update_entities', 'ue_models')
ue_root_dir = parser.get('update_entities', 'ue_root_dir')
ue_clear_flag = parser.get('update_entities', 'ue_clear_flag')

# Rails config
sign_in = parser.get('rails_config', 'sign_in')
druid_req = parser.get('rails_config', 'druid_req')
login_email = parser.get('rails_config', 'email')
login_password = parser.get('rails_config', 'password')
datasources = parser.get('rails_config', 'datasources')
entities = parser.get('rails_config', 'entities')

# Employee Roaster
er_check_connection = parser.get('employee_roaster', 'er_check_connection')

# User Creation
webui_url = parser.get('whizAPI', 'webui_url')
login_url = parser.get('whizAPI', 'login_url')
datasource_url = parser.get('whizAPI', 'datasource_url')
getusers_url = parser.get('whizAPI', 'getusers_url')
datasource_id = parser.get('whizAPI', 'datasource_id')
cu_sql_file = parser.get('whizAPI', 'cu_sql_file')
web_channel_url = parser.get('whizAPI', 'web_channel_url')

# Move Files
source_path = parser.get('filepath', 'source_path')
file_source_variable = parser.get('filenames', 'file_source_variable')
metadata_source = parser.get('file_move', 'metadata_source')
metadata_dest = parser.get('file_move', 'metadata_dest')


# User Access
ua_source_file_path = parser.get('user_access', 'ua_source_file_path')
ua_base_path = parser.get('user_access', 'ua_base_path')

whiz_user_file = parser.get('user_access', 'whiz_user_file')
jnj_user_file = parser.get('user_access', 'jnj_user_file')

ua_new_users = parser.get('user_access', 'ua_new_users')
ua_deactivate_users = parser.get('user_access', 'ua_deactivate_users')
ua_reactivate_users = parser.get('user_access', 'ua_reactivate_users')
ua_default_password = parser.get('user_access', 'ua_default_password')
ua_channel_type = parser.get('user_access', 'ua_channel_type')
ua_default_page = parser.get('user_access', 'ua_default_page')

# Data Validation
dv_stat_file = parser.get('data_validation', 'dv_stat_file')
dv_config_file = parser.get('data_validation', 'dv_config_file')
dv_result_file = parser.get('data_validation', 'dv_result_file')
dv_query_file = parser.get('data_validation', 'dv_query_file')

# mail
recipients = parser.get('data_validation_mail', 'recipients')
sender = parser.get('data_validation_mail', 'sender')
subject = parser.get('data_validation_mail', 'subject')
body = parser.get('data_validation_mail', 'body')
success_body = parser.get('data_validation_mail', 'success_body')
mail_server = parser.get('data_validation_mail', 'mail_server')

