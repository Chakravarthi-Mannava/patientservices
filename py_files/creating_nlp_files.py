import pandas as pd
import os
import json
import sys
import re
import tarfile
from whizpy.framework.solution.py_files import config
import shutil

nlpfilessource_dir = 'dags/whizpy/framework/solution/inputs/nlp'
nlpfiles_dir = 'dags/whizpy/framework/solution/outputs/nlp/askensemble/'

def clean_data(data, col):
    data[col] = data[col].apply(str)
    data[col] = data[col].map(lambda x: x.strip())


def make_tarfile(output_filename, input_file):
    with tarfile.open(config.backup_folder + output_filename, "w:gz") as tar:
        tar.add(input_file)


def nlp_process(raw_data, x, class_name):
    try:
        data = raw_data[raw_data['metadata'] == x].copy()
        del data['metadata']
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_0'] != 'NaN']
        data = data[data['entity_0'] != '']
        data['CLASS'] = class_name
        data['CODE'] = data['entity_0']
        data['NAME'] = data['entity_0']
        data['DESC'] = data['entity_0']
        data['LEVEL'] = x
        data['SYN1'], data['SYN2'], data['SYN3'], data['SYN4'], data['SYN5'], data['SYN6'], data['SYN7'], data['SYN8'], \
        data['SYN9'], data['SYN10'], data['SYN11'], data['SYN12'], data['SYN13'], data[
            'SYN14'] = '', '', '', '', '', '', '', '', '', '', '', '', '', ''

        data = data[data.columns.drop(list(data.filter(regex='entity')))]

    except Exception as e:
        print("Unexpected error:", sys.exc_info()[0])
        print("Failed to Generate, Please check the following File:" + x)
        sys.exit()
    else:
        return data


def acronym_nlp_process(raw_data, x, class_name):
    print("acronym process")
    try:  # level = x
        data = raw_data[raw_data['metadata'] == x].copy()
        del data['metadata']
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_0'] != 'NaN']
        data = data[data['entity_0'] != '']
        data['CLASS'] = class_name
        data['CODE'] = data['entity_0']
        data['NAME'] = data['entity_0']
        data['DESC'] = data['entity_0']
        data['LEVEL'] = x
        data = data.drop('entity_0', axis=1)
        for col in [x for x in data.columns if 'entity' in x]:
            clean_data(data, col)
        data = data.rename(columns=lambda x: re.sub('entity_', 'SYN', x))
        for name in [f'SYN{i}' for i in range(1, 15)]:
            if name in data:
                print(name + ' exist')
            else:
                data[name] = ''
    except Exception as e:
        print(e)
        print("Failed to Generate, Please check the following File:" + x)
        sys.exit()
    else:
        return data


def lookup_nlp_process(raw_data, x, class_name):
    print("lookup process")
    try:
        data = raw_data[raw_data['metadata'] == x].copy()
        del data['metadata']
        data = data.dropna()
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_0'] != 'NaN']
        data = data[data['entity_0'] != '']
        data['entity_1'] = data['entity_1'].apply(str)
        data['entity_1'] = data['entity_1'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_1'] != 'NaN']
        data = data[data['entity_1'] != '']
        data['CLASS'] = class_name
        data['CODE'] = data['entity_0']
        data['NAME'] = data['entity_1']
        data['DESC'] = data['entity_1']
        data['LEVEL'] = x
        data['SYN1'] = data['entity_0']

        data['SYN2'] = data['NAME'] + " (" + data['CODE'].astype(str) + ")"

        data['SYN3'], data['SYN4'], data['SYN5'], data['SYN6'], data['SYN7'], data['SYN8'], data['SYN9'], data['SYN10'], \
        data['SYN11'], data['SYN12'], data['SYN13'], data['SYN14'] = '', '', '', '', '', '', '', '', '', '', '', ''
        data = data.drop('entity_0', axis=1)
        data = data.drop('entity_1', axis=1)
    except Exception as e:
        print(e)
        print("Failed to Generate, Please check the following File:" + x)
        sys.exit()
    else:
        return data


def nlp_tags_process(raw_data, x, class_name):
    try:
        print(raw_data.head())
        print(x)
        data = raw_data[raw_data['metadata'] == x]
        del data['metadata']
        if 'entity_1' in data.columns:
            del data['entity_1']

        print(data.head())
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda t: t.strip())
        expand_element = pd.DataFrame(data['entity_0'].str.split('|').tolist()).stack()

        expand_element = expand_element.reset_index(drop=True)

        data = pd.DataFrame(expand_element, columns=['dummy_value'])

        data = data.drop_duplicates()
        data = data[data['dummy_value'] != 'NaN']
        data = data[data['dummy_value'] != '']
        data['CLASS'] = class_name
        data['CODE'] = data['dummy_value']
        data['NAME'] = data['dummy_value']
        data['DESC'] = data['dummy_value']
        data['LEVEL'] = x
        data['SYN1'], data['SYN2'], data['SYN3'], data['SYN4'], data['SYN5'], data['SYN6'], data['SYN7'], data['SYN8'], \
        data['SYN9'], data['SYN10'], data['SYN11'], data['SYN12'], data['SYN13'], data[
            'SYN14'] = '', '', '', '', '', '', '', '', '', '', '', '', '', ''
        data = data.drop('dummy_value', axis=1)
    except Exception as e:
        print(e)
        print("Failed to Generate, Please check the following File:" + x)
    return data


def append_to_existing(data_final, file):
    initial = pd.read_csv(nlpfiles_dir + file + '.csv', sep='\t')
    initial = initial.drop_duplicates(subset=['CODE'])
    df_new = initial[['CODE', 'LEVEL']].merge(data_final.drop_duplicates(), on=['CODE', 'LEVEL'],
                                              how='right', indicator=True)
    df_new = df_new[df_new['_merge'] != 'both']
    del df_new['_merge']
    print("df_new", df_new.shape)

    initial = initial.sort_index(by='ID', ascending=True).reset_index(drop=True)
    maxidx = initial['ID'].iloc[-1]
    print("length", len(df_new))
    df_new['ID'] = pd.RangeIndex(maxidx + 1, maxidx + 1 + len(df_new))
    print(df_new)
    print(set(initial.columns.values.tolist()) == set(df_new.columns.values.tolist()))
    df_final = pd.concat([initial, df_new])
    df_final = df_final[df_final['CODE'] != '']
    df_final = df_final[
        ['ID', 'CLASS', 'CODE', 'NAME', 'DESC', 'LEVEL', 'SYN1', 'SYN2', 'SYN3', 'SYN4', 'SYN5', 'SYN6', 'SYN7', 'SYN8',
         'SYN9', 'SYN10', 'SYN11', 'SYN12', 'SYN13', 'SYN14']]
    print(df_final.shape)
    df_final.to_csv(nlpfiles_dir + file + '.csv', sep='\t', index=False)


def attribute_nlp_process(raw_data, x, parent, class_name):
    try:
        data = raw_data[raw_data['metadata'] == x].copy()
        del data['metadata']
        if 'entity_1' in data.columns:
            del data['entity_1']
        data = data.dropna()
        print("class name", class_name)
        print("parent", parent)
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_0'] != 'NaN']
        data = data[data['entity_0'] != '']
        print(data)
        data['CLASS'] = class_name
        data['CODE'] = data['entity_0']
        data['NAME'] = data['entity_0']
        data['DESC'] = data['entity_0']
        data['PARENT'] = parent
        data['LEVEL'] = x
        data['SYN1'], data['SYN2'], data['SYN3'], data['SYN4'], data['SYN5'], data['SYN6'], data['SYN7'], data['SYN8'], \
        data['SYN9'], data['SYN10'], data['SYN11'], data['SYN12'], data['SYN13'], data[
            'SYN14'] = '', '', '', '', '', '', '', '', '', '', '', '', '', ''
        data = data[data.columns.drop(list(data.filter(regex='entity')))]
        print(data)
    except Exception as e:
        print("Unexpected error:", sys.exc_info()[0])
        print("Failed to Generate, Please check the following File:" + x)
        sys.exit()
    else:
        return data


def keyvalue_nlp_process(raw_data, x, class_name):
    # print("Key Value process")
    try:  # level = x
        data = raw_data[raw_data['metadata'] == x].copy()
        # print(data)
        del data['metadata']
        data = data.dropna()
        data['entity_0'] = data['entity_0'].apply(str)
        data['entity_0'] = data['entity_0'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_0'] != 'NaN']
        data = data[data['entity_0'] != '']
        data['entity_1'] = data['entity_1'].apply(str)
        data['entity_1'] = data['entity_1'].map(lambda x: x.strip())
        data = data.drop_duplicates()
        data = data[data['entity_1'] != 'NaN']
        data = data[data['entity_1'] != '']
        data['CLASS'] = class_name
        # data['CODE'] = data['entity_1'].str
        data['CODE'] = data['entity_1']
        data['NAME'] = data['entity_0']
        data['DESC'] = data['entity_0']
        data['LEVEL'] = x
        data['SYN1'] = data['entity_1']
        ######## Merging CODE+NAME as synonyms
        data['SYN2'] = data['NAME'] + " (" + data['CODE'].astype(str) + ")"
        data['SYN3'] = data['NAME'] + " (" + data['SYN1'].astype(str) + ")"
        data['NAME'] = data['NAME'] + " (" + data['SYN1'].astype(str) + ")"
        data['SYN4'] = data['entity_0']
        data['SYN5'], data['SYN6'], data['SYN7'], data['SYN8'], data['SYN9'], data['SYN10'], \
        data['SYN11'], data['SYN12'], data['SYN13'], data['SYN14'] = '', '', '', '', '', '', '', '', '', ''
        data = data.drop('entity_0', axis=1)
        data = data.drop('entity_1', axis=1)
    except Exception as e:
        print(e)
        print("Failed to Generate, Please check the following File:" + x)
        sys.exit()
    else:
        return data

def creating_nlp_files():

    append_to_existing_files = []

    if os.path.isdir(nlpfiles_dir) == False:
        os.mkdir(nlpfiles_dir)
    files = ['Metadata.csv', 'Metrics.csv', 'Computation.csv', 'Currency.csv']
    for f in files:
        shutil.copy('{nlpbkp}/{fi}'.format(nlpbkp=nlpfilessource_dir, fi=f), nlpfiles_dir)
    with open('{configpath}/{configfile}'.format(configpath=nlpfilessource_dir,
                                                 configfile=config.nlpconfig)) as json_data:
        nlp_config = json.load(json_data)
        json_data.close()
    nlp_preprocessing_file = pd.DataFrame()
    for index, columns in enumerate(nlp_config['preprocessing']):
        print(columns)
        entities = pd.DataFrame()
        for index1, file in enumerate(columns['files']):
            print(columns['column'])
            temp_entities = pd.read_csv(file, sep="\t", usecols=columns['column'])[columns['column']]
            print(temp_entities.columns)

            temp_entities.rename(columns={x: "entity_" + str(y) for x, y in zip(temp_entities.columns,
                                                                                range(0, len(temp_entities.columns)))},
                                 inplace=True)

            temp_entities['metadata'] = columns['column'][0]
            entities = entities.append(temp_entities)
            print(entities.columns)

        entities = entities.drop_duplicates().reset_index(drop=True)
        nlp_preprocessing_file = nlp_preprocessing_file.append(entities)
    nlp_preprocessing_file.fillna('', inplace=True)
    nlp_preprocessing_file.to_csv('{nlpsource}/{nlp_preprocess}'.format(nlpsource=nlpfilessource_dir,
                                                                        nlp_preprocess=config.nlp_preprocessing_file),
                                  index=False)

    dimensions_to_lookup = [columns['column'][0] for index, columns in enumerate(nlp_config['preprocessing'])
                            if columns['type'] == 'lookup']
    dimensions_to_tag_process = [columns['column'][0] for index, columns in enumerate(nlp_config['preprocessing'])
                                 if columns['type'] == 'tag_process']
    dimensions_acronym = [columns['column'][0] for index, columns in enumerate(nlp_config['preprocessing'])
                          if columns['type'] == 'acronym']
    # Key Value
    dimensions_keyvalue = [columns['column'][0] for index, columns in enumerate(nlp_config['preprocessing'])
                           if columns['type'] == 'keyvalue']
    # Key Value ends
    # Attribute Starts
    dimensions_attribute = [columns['column'][0] for index, columns in enumerate(nlp_config['preprocessing'])
                            if columns['type'] == 'attribute']
    # Attribute Ends
    # Prepare NLP files
    tot = [x["file_name"] for x in nlp_config["files"]]
    for index, file in enumerate(tot):
        print(file)
        print(nlp_config["files"][index]["db_id"])
        # declared variable for Attribute
        dimension_child = ''
        data_final = pd.DataFrame()
        for dimension in nlp_config["files"][index]["dimensions"]:
            print(dimension)
            if isinstance(dimension, dict):
                dimension_child = dimension['child']
                dimension_parent = dimension['parent']
            if dimension in dimensions_to_lookup:
                print("started market name processing:")
                data_prod = lookup_nlp_process(nlp_preprocessing_file, dimension, file)
                print(data_prod.head())
            elif dimension in dimensions_to_tag_process:
                print("started market name processing:")
                data_prod = nlp_tags_process(nlp_preprocessing_file, dimension, file)
                print(data_prod.head())
            elif dimension_child in dimensions_attribute:
                data_prod = attribute_nlp_process(nlp_preprocessing_file, dimension_child, dimension_parent, file)
                print(data_prod.head())
            elif dimension in dimensions_keyvalue:
                data_prod = keyvalue_nlp_process(nlp_preprocessing_file, dimension, file)
            elif dimension in dimensions_acronym:
                data_prod = acronym_nlp_process(nlp_preprocessing_file, dimension, file)
            else:
                data_prod = nlp_process(nlp_preprocessing_file, dimension, file)
                print(data_prod.head())
            data_final = data_final.append(data_prod)
        print(data_final.shape)
        db_id = nlp_config["files"][index]["db_id"]
        data_final.insert(1, 'ID', range(db_id, db_id + len(data_final)))

        c = ['ID', 'CLASS', 'CODE', 'NAME', 'DESC', 'PARENT', 'LEVEL', 'SYN1', 'SYN2', 'SYN3', 'SYN4', 'SYN5', 'SYN6',
             'SYN7',
             'SYN8', 'SYN9', 'SYN10', 'SYN11', 'SYN12', 'SYN13', 'SYN14']
        data_final.columns = pd.CategoricalIndex(data_final.columns, categories=c, ordered=True)
        data_final = data_final.sort_index(axis=1)
        # print(df1)
        # data_final = data_final[
        #    ['ID', 'CLASS', 'CODE', 'NAME', 'DESC', 'LEVEL', 'SYN1', 'SYN2', 'SYN3', 'SYN4', 'SYN5', 'SYN6', 'SYN7',
        #     'SYN8', 'SYN9', 'SYN10', 'SYN11', 'SYN12', 'SYN13', 'SYN14']]
        data_final = data_final[data_final['CODE'] != '']
        data_final = data_final[data_final['CODE'] != 'NA']

        # Exclude Value logic starts
        '''tmp = nlp_config["files"][index]
        if "exclude_values" in tmp:
            exclude_val = tmp["exclude_values"]
            for val in exclude_val:
                data_final = data_final[data_final['CODE'] != val]'''
        tmp = nlp_config["files"][index]
        if "exclude_values" in tmp:
            exclude_val = tmp["exclude_values"]
            for collist in exclude_val:
                col = list(collist.keys())[0]
                exclude_val_list = list(collist.values())[0]
                data_final = data_final[~(data_final['CODE'].isin(exclude_val_list) & data_final['LEVEL'].isin([col]))]

        # Exclude Value logic Ends

        if file + '.csv' in append_to_existing_files:
            append_to_existing_new(data_final, file)
        else:
            data_final.to_csv(nlpfiles_dir + file + '.csv', sep='\t', index=False)
            print(nlpfiles_dir + file + '.csv')

def append_to_existing_new(data_final, file):
    initial = pd.read_csv(nlpfiles_dir + file + '.csv', sep='\t')
    initial = initial.drop_duplicates(subset=['CODE'])

    df_new = initial.append(data_final, ignore_index=True)
    print(len(initial))
    print(len(data_final))
    print('APPEND TO FUNCTION')
    print(df_new.head())
    print(len(df_new))

    df_new = df_new.drop_duplicates(subset=['CODE', 'LEVEL'], keep='first')

    df_new = df_new.sort_index(by='ID', ascending=True).reset_index(drop=True)
    max_idx = max(df_new['ID'])
    min_inx = min(df_new['ID'])
    print("length", len(df_new))
    df_new['ID'] = pd.RangeIndex(min_inx + 1, min_inx + 1 + len(df_new))

    df_new.to_csv(nlpfiles_dir + file + '_edit.csv', sep='\t', index=False)